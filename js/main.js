"use strict"

// REVEAL EVENTS AND CONFIGS

Reveal.addEventListener('slidechanged', function(event) {

    updateIframeOnResizeWindow();

    // $(event.currentSlide).find("iframe")[0].src = "iframes/map1.html";
    // clearTimeout(timers[$(event.currentSlide).attr("timestamp")]);

    // var idPrev = new Date().getTime();
    // $(event.previousSlide).attr("timestamp", idPrev);                

    // var prevSlideTimer = function($sel, callback){
    //     var timeout_id = setTimeout(function(){
    //         // var $sel = $("section[timestamp]"+idPrev)
    //         $sel.attr("timestamp", "");
    //         callback();
    //         clearTimeout(timeout_id)
    //     }, 1000);
    //     timers[idPrev] = timeout_id;

    // }
    // prevSlideTimer($(event.previousSlide), function(){
    //     $(event.previousSlide).find("iframe")[0].src = "";
    // });
    
    // console.log(event)
    // console.log(event.currentSlide)
    // console.log(event.previousSlide);

    removeEditOnAllSlides();

 } );

Reveal.addEventListener('overviewhidden', removeSlideMoving)

Reveal.addEventListener('overviewshown', function(event) {
    if (isEditMode()){
        addSlideMoving();
    }
 })

// IFrames Update

var timers = {};

var updateIframeOnResizeWindow = function(){
    var slide = Reveal.getCurrentSlide();
    var zoom = $(".slides").css("zoom");

    var iframes = $(slide).find("iframe");
    $.each(iframes, function(i,v){
        var innerDoc = v.contentDocument || v.contentWindow.document;
        innerDoc.getElementsByTagName("body")[0].style.zoom = zoom;        
    })    
 }

$(window).bind('resize', updateIframeOnResizeWindow);

// ==============================


// Overview and moving slides

var addSlideMoving = function(){
    var $present = $("section:not(.stack)");

    var movingArrows = '<div class="movingArrows">\
            <div id="upMove"></div>\
            <div id="leftMove"></div>\
            <div id="rightMove"></div>\
            <div id="downMove"></div>\
            <div id="leftMerge"></div>\
            <div id="rightMerge"></div>\
        </div>';

    var $movingArrows = $(movingArrows);
    $present.append($movingArrows);
 }

var removeSlideMoving = function(){
    $(".movingArrows").remove();
 }


// Adding slides

var twistSlides = function(){

    var ind = Reveal.getIndices();

    if (!ind.h){
        Reveal.right();
        Reveal.left();
        return;
    }

    Reveal.left();
    Reveal.right();
 }

var addSlide = function(templ, direction){

    var $present = $("section.present:not(.stack)");
    var $presentStack = $("section.present.stack");

    // console.log($present.length, $presentStack.length)
    // console.log(Reveal.getIndices())

    var $section = $("<section>Title</section>");
    var $wrapSection = $("<section></section>");

    if ($present.length && $presentStack.length){ // list in stack
        switch (direction){
            case "left":
                $presentStack.before($section); 
                twistSlides();
                return;

            case "up":
                $present.before($section); 
                twistSlides();
                return;

            case "down":
                $present.after($section); 
                twistSlides();
                Reveal.navigateDown();
                return;

            default: // right
                $presentStack.after($section); 
                Reveal.navigateRight();
                return;
        }
    }

    if (!$presentStack.length){ // individual, no stack

        switch (direction){
            case "left":
                $present.before($section); 
                twistSlides();
                return;

            case "up":
                $present.wrap($wrapSection);
                $present.before($section);
                twistSlides();
                return;

            case "down":
                $present.wrap($wrapSection);
                $present.after($section);
                twistSlides();
                Reveal.navigateDown();
                return;

            default: // right
                $present.after($section); 
                Reveal.navigateRight();
                return;
        }
    }
 }

// Format output

function formatXml(xml) {
    var formatted = '';
    var reg = /(>)(<)(\/*)/g;
    xml = xml.replace(reg, '$1\r\n$2$3');
    var pad = 0;
    jQuery.each(xml.split('\r\n'), function(index, node) {
        var indent = 0;
        if (node.match( /.+<\/\w[^>]*>$/ )) {
            indent = 0;
        } else if (node.match( /^<\/\w/ )) {
            if (pad != 0) {
                pad -= 1;
            }
        } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {
            indent = 1;
        } else {
            indent = 0;
        }

        var padding = '';
        for (var i = 0; i < pad; i++) {
            padding += '  ';
        }

        formatted += padding + node + '\r\n';
        pad += indent;
    });

    return formatted;
 }

var getSource = function(){
    var $slides = $(".slides");
    var html = $slides.html();

    html = html.replace(/class=".*?"/g, "");
    html = html.replace(/style=".*?"/g, "");
    html = html.replace(/hidden=""/g, "");
    html = html.replace(/data-previous-indexv=".+?"/g, "");
    html = html.replace(/data-index-h=".+?"/g, "");
    html = html.replace(/data-index-v=".+?"/g, "");
    // html = html.replace(/\s+/g, ""); // так какого-то жера работает форматирование
    html = html.replace(/\s+/g, " ");
    html = html.replace(/\s+?>/g, ">");

    return formatXml(html);
 }


// Edit

var addEditOnSlide = function(){
    var $present = $("section.present:not(.stack)");
    var $wrapSection = $('<div class="wrapDivContentEditable" contenteditable="true"></div>'); 

    $present.wrapInner($wrapSection);

    $(".wrapDivContentEditable").ckeditor();
 }

var removeEditOnAllSlides = function(){
    var $present = $(".wrapDivContentEditable");

    $(".cke").remove();

    $.each($present, function(i, v){
        var $v = $(v);
        $v.parent().html($v.html());
    })
 }

var isEditMode = function(){
    return $("#editPanel").is(":visible");
 }

var toggleEditMode = function(){
    $("#editPanel").toggleClass("shown hidden");
    $("#editModeOn").toggleClass("shown hidden");
 }

var toggleEditSlide = function(){
    if (!isEditMode()){ return }

    if ($(".wrapDivContentEditable").length){
        removeEditOnAllSlides();
    }
    else {
        addEditOnSlide();
    }
 }


// Delete slide

var deleteSlide = function(){

    var $present = $("section.present:not(.stack)");
    var $presentStack = $("section.present.stack");

    var slideContent = $present.html();

    $present.remove();

    if (!$presentStack.children().length){
        $presentStack.remove();
    }

    twistSlides();

    return slideContent;
 }


// EVENTS

$("#addSlideRight").bind("click", function(){addSlide("", "right")});
$("#addSlideLeft").bind("click", function(){addSlide("", "left")});
$("#addSlideUp").bind("click", function(){addSlide("", "up")});
$("#addSlideDown").bind("click", function(){addSlide("", "down")});

$("#editModeOn, #editModeOff").bind("click", toggleEditMode);
$("#editSlide").bind("click", toggleEditSlide);

$("#deleteSlide").bind("click", deleteSlide);

$("#getSource").bind("click", function(){ console.log(getSource()) })

$("#upMove").bind("click", function () { });
$("#leftMove").bind("click", function () { });
$("#rightMove").bind("click", function () { });
$("#downMove").bind("click", function () { });
$("#leftMerge").bind("click", function () { });
$("#rightMerge").bind("click", function () { });